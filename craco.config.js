const { whenProd, getPlugin, pluginByName } = require('@/craco/craco')

module.exports = {
  webpack: {
    configure: webpackConfig => {
      let cdn = {
        js: [],
        css: []
      }
      whenProd(() => {
        // 只会在生产环境执行
        webpackConfig.externals = {
          react: 'React',
          'react-dom': 'ReactDom',
          redux: 'Redux'
        }
        cdn = {
          js: [
            'https://cdn.bootcdn.net/ajax/libs/react/18.2.0/cjs/react-jsx-dev-runtime.production.min.js',
            'https://cdn.bootcdn.net/ajax/libs/react-dom/18.2.0/cjs/react-dom-server-legacy.browser.production.min.js',
            'https://cdn.bootcdn.net/ajax/libs/redux/4.2.1/redux.min.js'
          ],
          css: []
        }
      })
      const { isFound, match } = getPlugin(
        webpackConfig,
        pluginByName('HtmlWebpackPlugin')
      )
      if (isFound) {
        match.options.cdn = cdn
      }
      return webpackConfig
    }
  }
}
