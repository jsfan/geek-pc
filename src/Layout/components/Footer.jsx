import React, { Component } from "react";
import { Layout } from "antd";
import footer from "../styles/footer.module.scss";

const { Footer } = Layout;
export default class index extends Component {
  render() {
    return (
      <Footer className={footer.container}>
        <div className={footer.title}>
          <span>Copyright © 2023-2033 React Geek. All Rights Reserved.</span>
          <a
            href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=11000002000001"
            target="_blank"
          >
            <img
              src="https://www.2345.com/images/index/renzheng_gab.png"
              alt=""
            />
            京海市公安局备案10088998899号
          </a>
        </div>
      </Footer>
    );
  }
}
