import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Layout, Button, Popconfirm } from 'antd'
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  LogoutOutlined
} from '@ant-design/icons'
import header from '../styles/header.module.scss'
import MyBreadcrumb from './MyBreadcrumb'

const { Header } = Layout

// 获取redux中的settings中的state
const mapStateToProps = state => {
  return {
    isCollapsed: state.settings.isCollapsed
  }
}

// 获取redux中的settings中的action
const mapDispatchToProps = dispatch => {
  return {
    setCollapsed () {
      dispatch({ type: 'settings/setCollapsed' })
    },
    setToken (value = '') {
      dispatch({ type: 'user/setToken', value })
    }
  }
}

@connect(mapStateToProps, mapDispatchToProps)
class index extends Component {
  logout = () => {
    this.props.setToken()
  }
  render () {
    return (
      <Header className={header.container}>
        <Button
          type='primary'
          onClick={this.props.setCollapsed}
          style={{ marginBottom: 16 }}
        >
          {this.props.isCollapsed ? (
            <MenuUnfoldOutlined />
          ) : (
            <MenuFoldOutlined />
          )}
        </Button>
        <MyBreadcrumb></MyBreadcrumb>
        <Popconfirm
          title='确认退出登录?'
          description='Are you sure to delete this task?'
          onConfirm={this.logout}
          okText='确认'
          cancelText='取消'
        >
          <Button
            className={header.logout}
            icon={<LogoutOutlined />}
            type='primary'
          >
            退出登录
          </Button>
        </Popconfirm>
      </Header>
    )
  }
}

export default index
