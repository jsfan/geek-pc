import React, { useEffect, useState } from 'react'
import { useLocation } from 'react-router'
import routes from '@/router/modules'
import { Breadcrumb } from 'antd'
import { Link } from 'react-router-dom'

export default function MyBreadcrumb () {
  const location = useLocation()
  // 面包屑集合
  const [breadcrumbList, setBreadcrumbList] = useState([
    {
      title: <Link to='/home'>首页</Link>,
      name: '/home'
    }
  ])
  useEffect(() => {
    createBreadcrumb(location.pathname)
  }, [location])
  // 计算生成新面包屑
  const createBreadcrumb = path => {
    const index = breadcrumbList.findIndex(item => item.name === path)
    if (index !== -1) return
    routes.forEach(item => {
      if (item.key === path) {
        setBreadcrumbList([
          ...breadcrumbList,
          { title: <Link to={item.key}>{item.label}</Link>, name: item.key }
        ])
      } else {
        item.children &&
          item.children.forEach(item => {
            if (item.key === path) {
              setBreadcrumbList([
                ...breadcrumbList,
                {
                  title: <Link to={item.key}>{item.label}</Link>,
                  name: item.key
                }
              ])
            }
          })
      }
    })
  }
  return (
    <div style={{ display: 'inline-block', marginLeft: '20px' }}>
      <Breadcrumb items={breadcrumbList}></Breadcrumb>
    </div>
  )
}
