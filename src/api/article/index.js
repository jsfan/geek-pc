import request from '@/axios/request'

export const submitArticle = (draft, data) => {
  return request({
    url: '/mp/articles?draft=' + draft,
    method: 'post',
    data
  })
}

export const editSubmitArticle = (id, draft, data) => {
  return request({
    url: `/mp/articles/${id}?draft=` + draft,
    method: 'put',
    data
  })
}

export const deleteArticle = id => {
  return request({
    url: '/mp/articles/' + id,
    method: 'delete'
  })
}

export const getArticleDetail = id => {
  return request({
    url: '/mp/articles/' + id,
    method: 'get'
  })
}
