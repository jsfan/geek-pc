import request from '@/axios/request';

export const login = (data) => {
    return request({
        url: '/authorizations',
        method: 'post',
        data
    })
}