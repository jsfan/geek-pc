import axios from 'axios'
import { message } from 'antd'
import store from '@/store'
import { BASE_URL, TIMEOUT } from './config'
import { getToken } from '@/utils'

const Interface = axios.create({
  baseURL: BASE_URL,
  timeout: TIMEOUT
})

// 请求拦截器
Interface.interceptors.request.use(
  config => {
    const token = getToken()
    config.headers['Authorization'] = `Bearer ${token}`
    return config
  },
  error => {
    return error
  }
)

Interface.interceptors.response.use(
  res => {
    if ([200, 201].includes(res.status)) {
      return res.data
    } else if (res.status === 400) {
      store.dispatch({ type: 'user/setToken', value: '' })
    } else {
      message.error('This is an error message')
      return Promise.reject()
    }
  },
  error => {
    console.info('err', error)
    message.error(error.response.data.message)
    if (error.response.status === 401) {
      store.dispatch({ type: 'user/setToken', value: '' })
    } else {
      return error
    }
  }
)

export default Interface
