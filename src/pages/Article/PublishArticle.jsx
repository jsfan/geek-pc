import React, { useEffect, useState, useRef } from 'react'
// hooks
import { useNavigate, useParams } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
// 组件和样式
import {
  Form,
  Input,
  Radio,
  Select,
  message,
  Upload,
  Button,
  Space,
  Skeleton
} from 'antd'
import { PlusOutlined } from '@ant-design/icons'
import ReactQuill from 'react-quill'
import 'quill/dist/quill.snow.css'
// action
import { getChannels } from '@/store/actions/article'
import {
  submitArticle,
  getArticleDetail,
  editSubmitArticle
} from '@/api/article'

export default function PublishArticle () {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  // 表单配置对象
  const [form] = Form.useForm()
  const formConfig = {
    form: {
      name: 'form',
      form: form,
      initialValues: {
        type: 1
      },
      layout: 'horizontal',
      labelCol: {
        span: 4
      },
      wrapperCol: {
        span: 14
      }
    },
    formItem: {
      title: {
        name: 'title',
        label: '标题',
        rules: [{ required: true, message: '请填写标题' }]
      },
      channels: {
        name: 'channel_id',
        label: '频道',
        rules: [{ required: true, message: '请选择频道' }]
      },
      type: {
        name: 'type',
        label: '封面'
      },
      content: {
        name: 'content',
        label: '内容',
        rules: [{ required: true, message: '请填写文章内容' }]
      }
    }
  }
  const noLabelwrapperCol = {
    wrapperCol: { span: 14, offset: 4 }
  }
  // 频道下拉
  useEffect(() => {
    dispatch(getChannels())
  }, [])
  const channels = useSelector(state =>
    state.article.channels.map(item => ({ label: item.name, value: item.id }))
  )
  // -----------------------------------<文件上传>------------------------------------------
  // 单选
  const radioList = [
    {
      label: '单图',
      value: 1
    },
    {
      label: '三图',
      value: 3
    },
    {
      label: '无图',
      value: 0
    }
  ]
  // 单选类型
  const [coverType, setCoverType] = useState(1)
  // 类型单选变化
  const onTypeChange = e => {
    console.log('fileRef.current', fileRef.current)
    setCoverType(e.target.value) // 1|3|0
    setFileList(fileRef.current.slice(0, e.target.value)) //fileRef.current：【1，2，3】
  }
  // 支持的文件的类型
  const FILETYPE = ['image/jpg', 'image/gif', 'image/jpeg', 'image/png']
  // 根据图片列表总数组过滤出来的，提交使用
  const [fileList, setFileList] = useState([])
  //图片列表总数组，仅做储存使用
  const fileRef = useRef(fileList)
  // upload组件参数
  const uploadProps = {
    name: 'file',
    listType: 'picture-card',
    action: 'http://toutiao.itheima.net/v1_0/upload',
    name: 'image',
    fileList,
    maxCount: coverType,
    beforeUpload (file) {
      const isJpgOrPng = FILETYPE.includes(file.type) //  isNAN()
      if (!isJpgOrPng) {
        message.error('You can only upload JPG/PNG file!')
      }
      const isLt2M = file.size / 1024 / 1024 < 2
      if (!isLt2M) {
        message.error('Image must smaller than 2MB!')
      }
      return isJpgOrPng && isLt2M
    },
    onChange ({ file, fileList }) {
      console.log(666, 'file, fileList', file, fileList)
      if (FILETYPE.includes(file.type) || file.status === 'removed') {
        setFileList(fileList) //fileList  =====所有的图片的集合
        fileRef.current = fileList
        console.log('fileRef.current', fileRef.current)
      }
    }
  }
  // -----------------------------------<文件上传 />-----------------------------------------
  // 富文本编辑器
  const quillOption = {
    toolbar: {
      container: [
        [{ header: [1, 2, 3, 4, 5, 6, false] }],
        ['bold', 'italic', 'underline', 'strike'],
        [{ list: 'ordered' }, { list: 'bullet' }],
        [{ align: [] }],
        [{ color: [] }, { background: [] }],
        ['link', 'image'],
        ['clean']
      ]
    }
  }
  // 发布
  const submit = async (id, draft) => {
    // 校验
    const valid = await form.validateFields().catch(e => false)
    if (!valid) return
    if (valid.type !== fileList.length) {
      return message.warning(`封面数量不为${valid.type}张`)
    }
    // 收集图片
    const images = fileList.map(item => {
      if (item.url) {
        return item.url
      } else {
        return item.response.data.url
      }
    })
    // 洗参
    const { type, ...rest } = valid
    const params = {
      ...rest,
      cover: {
        type,
        images
      }
    }
    console.log('params', params)
    if (id) {
      const res = await editSubmitArticle(id, draft, params)
      if (res.message === 'OK') message.success('保存成功')
    } else {
      const res = await submitArticle(draft, params)
      if (draft && res.message === 'OK') message.success('保存成功')
      if (!draft && res.message === 'OK') message.success('发布成功')
    }
    navigate('/article/list')
  }
  // 编辑回显
  const params = useParams()
  const [loading, setLoading] = useState(false)
  const getArticleDetailFn = async () => {
    setLoading(true)
    const res = await getArticleDetail(params.id)
    setLoading(false)
    form.setFieldsValue({ ...res.data, type: res.data.cover?.type })
    setCoverType(res.data.cover?.type)
    setFileList(res.data.cover?.images.map(url => ({ url })))
    fileRef.current = res.data.cover?.images.map(url => ({ url }))
    console.log('fileRef.current', fileRef.current)
  }
  useEffect(() => {
    if (params.id !== ':id') {
      getArticleDetailFn()
    }
  }, [])
  return (
    <div style={{ padding: '40px 200px' }}>
      {loading && (
        <div
          style={{
            position: 'fixed',
            left: '50%',
            transform: 'translateX(-50%)'
          }}
        >
          <Skeleton
            active
            paragraph={{
              rows: 6,
              width: [500, 500, 200, 100, 500, 200]
            }}
          ></Skeleton>
        </div>
      )}
      {!loading && (
        <Form {...formConfig.form}>
          <Form.Item {...formConfig.formItem.title}>
            <Input placeholder='请输入标题' />
          </Form.Item>
          <Form.Item {...formConfig.formItem.channels}>
            <Select placeholder='请选择频道' options={channels}></Select>
          </Form.Item>
          <Form.Item {...formConfig.formItem.type}>
            <Radio.Group onChange={onTypeChange} value={coverType}>
              {radioList.map(item => (
                <Radio key={item.value} value={item.value}>
                  {item.label}
                </Radio>
              ))}
            </Radio.Group>
          </Form.Item>
          <Form.Item {...noLabelwrapperCol}>
            <Upload {...uploadProps}>
              {fileList.length < coverType && <PlusOutlined />}
            </Upload>
          </Form.Item>
          <Form.Item {...formConfig.formItem.content}>
            <ReactQuill
              style={{ height: 300, marginBottom: 40 }}
              theme='snow'
              modules={quillOption}
            ></ReactQuill>
          </Form.Item>
          <Form.Item {...noLabelwrapperCol}>
            {params.id !== ':id' && (
              <Space>
                <Button type='primary' onClick={() => submit(params.id, false)}>
                  修改文章
                </Button>
                <Button onClick={() => submit(params.id, true)}>
                  存为草稿
                </Button>
              </Space>
            )}
            {params.id === ':id' && (
              <Space>
                <Button type='primary' onClick={() => submit(null, false)}>
                  发布文章
                </Button>
                <Button onClick={() => submit(null, true)}>存为草稿</Button>
              </Space>
            )}
          </Form.Item>
        </Form>
      )}
    </div>
  )
}
