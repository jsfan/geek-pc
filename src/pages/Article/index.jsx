import React, { useEffect, useState } from 'react'
// hooks
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router'
// action
import { getArticleList, getChannels } from '@/store/actions/article'
import { deleteArticle } from '@/api/article'
// 组件和样式
import {
  Table,
  Space,
  Tag,
  Button,
  Form,
  Radio,
  Select,
  DatePicker,
  Popconfirm
} from 'antd'
import {
  AlignRightOutlined,
  DeleteOutlined,
  EditOutlined
} from '@ant-design/icons'
import styles from './styles/index.module.scss'
import noImg from '@/assets/no-img.png'
import 'dayjs/locale/zh-cn'
import locale from 'antd/es/date-picker/locale/zh_CN'

export default function Index () {
  const dispatch = useDispatch()
  const navigate = useNavigate()
  // 查询
  const search = () => {
    dispatch(getArticleList(getTableParams))
  }
  // 表格
  const [getTableParams, setTableParams] = useState({
    page: 1,
    per_page: 10,
    status: '', // 状态
    channel_id: '', // 频道id
    begin_pubdate: '', // 开始时间
    end_pubdate: '' // 结束时间
  })
  useEffect(() => {
    search()
    dispatch(getChannels())
  }, [])
  const articleList = useSelector(state => state.article.articleList)
  const columns = [
    {
      title: '封面',
      dataIndex: 'cover',
      render: val => {
        if (val.type) {
          return (
            <div className={styles['img-box']}>
              <img src={val.images[0]}></img>
            </div>
          )
        } else {
          return (
            <div className={styles['img-box']}>
              <img src={noImg}></img>
            </div>
          )
        }
      }
    },
    {
      title: '标题',
      dataIndex: 'title'
    },
    {
      title: '审核状态',
      dataIndex: 'status',
      render: (_, { status, id }) => {
        let color = ''
        let tag = ''
        if (status === 0) {
          color = 'volcano'
          tag = '审核未通过'
        } else if (status === 1) {
          color = 'geekblue'
          tag = '审核中'
        } else if (status === 2) {
          color = 'green'
          tag = '审核通过'
        }
        return (
          <Tag color={color} key={id}>
            {tag}
          </Tag>
        )
      }
    },
    {
      title: '发布时间',
      dataIndex: 'pubdate'
    },
    {
      title: '阅读数',
      dataIndex: 'read_count'
    },
    {
      title: '评论数',
      dataIndex: 'comment_count'
    },
    {
      title: '点赞数',
      dataIndex: 'like_count'
    },
    {
      title: '操作',
      render: (_, { id }) => (
        <Space size='middle'>
          <Button
            type='primary'
            shape='circle'
            onClick={() => editFn(id)}
            icon={<EditOutlined />}
          ></Button>
          <Popconfirm
            title='确认删除?'
            description='Are you sure to delete this task?'
            onConfirm={() => deleteFn(id)}
            okText='确认'
            cancelText='取消'
          >
            <Button
              danger
              type='primary'
              shape='circle'
              icon={<DeleteOutlined />}
            ></Button>
          </Popconfirm>
        </Space>
      )
    }
  ]
  // 分页
  const total = useSelector(state => state.article.total)
  const onShowSizeChange = (current, pageSize) => {
    setTableParams(params => ({ ...params, page: current, per_page: pageSize }))
  }
  useEffect(() => {
    search()
  }, [getTableParams.page, getTableParams.per_page])
  // 删除
  const deleteFn = async id => {
    await deleteArticle(id)
    search()
  }
  // 编辑
  const editFn = id => {
    navigate('/article/publish/' + id)
  }
  // 状态单选
  const radioList = [
    {
      label: '全部',
      value: ''
    },
    {
      label: '草稿',
      value: 0
    },
    {
      label: '待审核',
      value: 1
    },
    {
      label: '审核通过',
      value: 2
    },
    {
      label: '审核失败',
      value: 3
    }
  ]
  // 频道下拉
  const channels = useSelector(state =>
    state.article.channels.map(item => ({ label: item.name, value: item.id }))
  )
  return (
    <div>
      {/* 筛选 */}
      <Form>
        <Form.Item label='状态'>
          <Radio.Group
            onChange={e =>
              setTableParams({ ...getTableParams, status: e.target.value })
            }
            value={getTableParams.status}
          >
            {radioList.map(item => (
              <Radio key={item.value} value={item.value}>
                {item.label}
              </Radio>
            ))}
          </Radio.Group>
        </Form.Item>
        <Form.Item label='频道'>
          <Select
            onChange={e => setTableParams({ ...getTableParams, channel_id: e })}
            style={{ width: 300 }}
            options={channels}
          ></Select>
        </Form.Item>
        <Form.Item label='日期'>
          <DatePicker.RangePicker
            locale={locale}
            placeholder={['开始日期', '结束日期']}
            onChange={(_, e) =>
              setTableParams({
                ...getTableParams,
                begin_pubdate: e[0],
                end_pubdate: e[0]
              })
            }
          ></DatePicker.RangePicker>
        </Form.Item>
        <Form.Item>
          <Button type='primary' icon={<AlignRightOutlined />} onClick={search}>
            筛选
          </Button>
        </Form.Item>
      </Form>
      {/* 表格 */}
      <Table
        rowKey={row => row.id}
        columns={columns}
        dataSource={articleList}
        pagination={{
          showSizeChanger: true,
          onChange: onShowSizeChange,
          current: getTableParams.page,
          pageSize: getTableParams.per_page,
          total
        }}
      ></Table>
    </div>
  )
}
