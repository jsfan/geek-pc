import React from 'react'
import { useDispatch } from 'react-redux'
import { useLocation, useNavigate } from 'react-router-dom'
import { Button, Checkbox, Form, Input, message } from 'antd'
import { login } from '@/api'
import { getParams } from '@/utils'

function App() {
  const dispatch = useDispatch()
  const location = useLocation()
  const navigate = useNavigate()
  // form info
  const [form] = Form.useForm()
  const formConfig = {
    username: {
      label: 'mobile',
      name: 'mobile',
      rules: [
        {
          required: true,
          message: 'Please input your mobile!'
        }
      ]
    },
    password: {
      label: 'password',
      name: 'code',
      rules: [
        {
          required: true,
          message: 'Please input your password!'
        }
      ]
    },
    agree: {
      wrapperCol: {
        offset: 8,
        span: 16
      },
      name: 'agree',
      valuePropName: 'checked',
      rules: [
        {
          validator: (_, value) => value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement'))
        }
      ]
    },
    login: {
      wrapperCol: {
        offset: 8,
        span: 16
      },
    }
  }
  const initialValues = {
    agree: true,
    mobile: '13911111111',
    code: '246810'
  }
  // login submit
  const handleLogin = async () => {
    const valid = await form.validateFields().catch(e => false)
    if (!valid) return
    const res = await login(valid)
    if(res.message === 'OK') {
      message.success('登录成功！')
    }
    const { redirect } = getParams(location.search)
    dispatch({ type: 'user/setToken', value: res.data.token })
    setTimeout(() => {
      navigate(redirect || '/')
    }, 0)
  }
  return (
    <Form
      form={form}
      style={{ paddingTop: '100px' }}
      name='form'
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 8 }}
      initialValues={initialValues}
      autoComplete='off'
    >
      <Form.Item {...formConfig.username}>
        <Input />
      </Form.Item>

      <Form.Item {...formConfig.password}>
        <Input.Password />
      </Form.Item>

      <Form.Item {...formConfig.agree}>
        <Checkbox>
          Remember me
        </Checkbox>
      </Form.Item>

      <Form.Item {...formConfig.login}>
        <Button type='primary' onClick={handleLogin}>
          Login
        </Button>
      </Form.Item>
    </Form>
  )
}

export default App
