import { lazy } from 'react'
import { ReadOutlined, FileDoneOutlined, FileAddOutlined } from '@ant-design/icons'
// import Article from '@/pages/Article'
// import PublishArticle from '@/pages/Article/PublishArticle'

const Article = lazy(() => import('@/pages/Article'))
const PublishArticle = lazy(() => import('@/pages/Article/PublishArticle'))

const ArticleRouter = [
    {
        sort: 3,
        key: '/article',
        label: '文章',
        icon: <ReadOutlined />,
        children: [
            {
                hidden: false,
                path: '/article/list',
                key: '/article/list',
                label: '文章列表',
                icon: <FileDoneOutlined />,
                Component: (props) =>  <Article {...props} />,
            },
            {
                hidden: false,
                path: '/article/publish/:id',
                key: '/article/publish/:id',
                label: '发布文章',
                icon: <FileAddOutlined />,
                Component: (props) =>  <PublishArticle {...props} />,
            }
        ]
    }
]

export default ArticleRouter