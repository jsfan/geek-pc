import request from '@/axios/request'
import { ARTICLE_SETARTICLELIST, ARTICLE_SETARTICLELISTCOUNT, ARTICLE_SERCHANNELS } from '../actionTypes'

export function getArticleList (params) {
  return async dispatch => {
    const res = await request({ url: '/mp/articles', method: 'get', params })
    dispatch({ type: ARTICLE_SETARTICLELIST, value: res.data.results })
    dispatch({ type: ARTICLE_SETARTICLELISTCOUNT, value: res.data.total_count })
  }
}

export function getChannels () {
  return async dispatch => {
    const res = await request({ url: '/channels' })
    dispatch({ type: ARTICLE_SERCHANNELS, value: res.data.channels })
  }
}
