import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  articleList: [],
  total: 0,
  channels: []
}

export const articleSlice = createSlice({
  name: 'article',
  initialState,
  reducers: {
    setArticleList (state, { value }) {
      state.articleList = value
    },
    setArticleListCount (state, { value }) {
      state.total = value
    },
    setChannels (state, { value }) {
      state.channels = value
    }
  }
})

export default articleSlice.reducer
