import { configureStore } from '@reduxjs/toolkit'

import settingSlice from './features/settings'
import userSlice from './features/user'
import articleSlice from './features/article'

const store = configureStore({
  reducer: {
    settings: settingSlice,
    user: userSlice,
    article: articleSlice
  }
})

export default store
